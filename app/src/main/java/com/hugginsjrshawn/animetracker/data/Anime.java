package com.hugginsjrshawn.animetracker.data;

import android.graphics.Bitmap;

/**
 * Created by Shawn on 1/1/2015.
 */


public class Anime {
    private int _id;
    private String title;
    private String description;
    private double score;
    private String genre;
    private String coverImage;
    private String episodeCount;
    private String startDate;
    private String endDate;
    private String status;
    private String url;
    private Bitmap image;


    public Anime() {
    }

    ;

    public Anime(String title, String description, double score, String genre) {

        this.title = title;
        this.description = description;
        this.score = score;
        this.genre = genre;
    }

    public Anime(String title, String description, double score, String genre, String coverImage,
                 String episodeCount, String startDate, String endDate, String status, String url) {

        this.title = title;
        this.description = description;
        this.score = score;
        this.genre = genre;
        this.coverImage = coverImage;
        this.episodeCount = episodeCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.url = url;
    }

    public String getAdditionalInfo() {
        //12 episodes 28 min Aired from 11 Oct 2014 to 27 Dec 2014
        //example string
        String info;
        if (startDate.equals("N/A")) {
            info = episodeCount + " episode(s), " + status;

        } else if (endDate.equals("N/A")) {
            info = episodeCount + " episodes, " + "Start date: " + startDate;

        } else {
            info = episodeCount + " episodes " + "Aired from " + startDate + " to " + endDate;
        }
        return info;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(String episodeCount) {
        this.episodeCount = episodeCount;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String image) {
        this.coverImage = image;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int id) {
        _id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getStringScore() {
        String scoreNumber = Double.toString(score);
        return scoreNumber = scoreNumber.substring(0, 3);
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}

