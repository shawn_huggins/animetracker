package com.hugginsjrshawn.animetracker.activites;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.fragments.AnimeDetailFragment;
import com.hugginsjrshawn.animetracker.fragments.AnimeListFragment;


public class AnimeListActivity extends ActionBarActivity implements AnimeListFragment.onAnimeSelectedListener {
    public static final String KEY_ID = "animelistactivity.keyid";
    private static final String TAG = "AnimeListActivity";
    private FragmentManager fm;
    private AnimeListFragment listFragment;

    private Boolean isDual = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        fm = getSupportFragmentManager();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (savedInstanceState == null) {
            fm.beginTransaction()
                    .add(R.id.container, new AnimeListFragment())
                    .commit();

            View view = findViewById(R.id.detail_container);
            if (view != null) {
                isDual = true;
                fm.beginTransaction().add(R.id.detail_container, new AnimeDetailFragment())
                        .commit();
            }
        }
    }

    @Override
    public void loadAnime(int AnimeId) {
        if (!isDual) {
            Intent i = new Intent(this, DetailAnimeActivity.class);
            i.putExtra(AnimeDetailFragment.KEY_ID, AnimeId);
            Log.d(TAG, "About to start DetailAnimeActivity");
            startActivity(i);
        } else {
            fm.beginTransaction().replace(R.id.detail_container,
                    AnimeDetailFragment.newInstance(AnimeId)).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_anime_list, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
