package com.hugginsjrshawn.animetracker.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hugginsjrshawn.animetracker.data.Contract.AnimeContract;

import java.util.ArrayList;

/**
 * Created by shawnjr on 12/25/14.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "DBHelper";

    private static final String DATABASE_CREATE = "CREATE TABLE " + AnimeContract.TABLE + " (_id integer primary key " +
            "autoincrement, " + AnimeContract._COVERIMAGE + " text not null, "
            + "title text not null UNIQUE ON CONFLICT REPLACE, description text not null, "
            + "genre text not null, score REAL DEFAULT 0.0, " +
            "episodecount text not null, startdate text not null, enddate text not null, " +
            "status text not null, url text not null);";
    ;


    //constructor
    public DBHelper(Context context) {
        super(context, Contract.DB_NAME, null, Contract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
        Log.d(TAG, "DATABASE created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Contract.AnimeContract.TABLE);
        onCreate(db);
    }

    //Create---------

    public void addAnime(Anime anime) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Contract.AnimeContract._TITLE, anime.getTitle());
        values.put(Contract.AnimeContract._DESCRIPTION, anime.getDescription());
        values.put(Contract.AnimeContract._GENRE, anime.getGenre());
        values.put(Contract.AnimeContract._SCORE, anime.getScore());
        values.put(AnimeContract._COVERIMAGE, anime.getCoverImage());
        values.put(AnimeContract._EPISODECOUNT, anime.getEpisodeCount());
        values.put(AnimeContract._STARTDATE, anime.getStartDate());
        values.put(AnimeContract._ENDDATE, anime.getEndDate());
        values.put(AnimeContract._STATUS, anime.getStatus());
        values.put(AnimeContract._URL, anime.getUrl());
        db.insert(AnimeContract.TABLE, null, values);
        Log.d(TAG, "Anime " + anime.getTitle() + " added");
        db.close();
    }

    //End Create --------

    //Read`-------------

    public Anime getAnimeById(int id) {
        int _id = id;
        SQLiteDatabase db = getReadableDatabase();
        Anime anime = new Anime();
        String query = "SELECT * FROM " + AnimeContract.TABLE + " WHERE " + AnimeContract._ID + " = " + id;
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            anime.set_id(c.getInt(c.getColumnIndex(AnimeContract._ID)));
            anime.setTitle(c.getString(c.getColumnIndex(AnimeContract._TITLE)));
            anime.setDescription(c.getString(c.getColumnIndex(Contract.AnimeContract._DESCRIPTION)));
            anime.setGenre(c.getString(c.getColumnIndex(AnimeContract._GENRE)));
            anime.setScore(c.getDouble(c.getColumnIndex(AnimeContract._SCORE)));
            anime.setCoverImage(c.getString(c.getColumnIndex(AnimeContract._COVERIMAGE)));
            anime.setEpisodeCount(c.getString(c.getColumnIndex(AnimeContract._EPISODECOUNT)));
            anime.setStartDate(c.getString(c.getColumnIndex(AnimeContract._STARTDATE)));
            anime.setEndDate(c.getString(c.getColumnIndex(AnimeContract._ENDDATE)));
            anime.setStatus(c.getString(c.getColumnIndex(AnimeContract._STATUS)));
            anime.setUrl(c.getString(c.getColumnIndex(AnimeContract._URL)));
        }
        c.close();
        db.close();
        return anime;

    }
    public ArrayList<Anime> getAllAnimeSearch(String title){
        String query = "SELECT * FROM " + AnimeContract.TABLE + " WHERE " + AnimeContract._TITLE + " LIKE" + " '%" +
                title  + "%'" + " ORDER BY " + AnimeContract._TITLE + " ASC";
        ArrayList<Anime> list = new ArrayList<Anime>();
        Anime anime;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                anime = new Anime();
                anime.set_id(c.getInt(c.getColumnIndex(AnimeContract._ID)));
                anime.setTitle(c.getString(c.getColumnIndex(AnimeContract._TITLE)));
                anime.setDescription(c.getString(c.getColumnIndex(AnimeContract._DESCRIPTION)));
                anime.setGenre(c.getString(c.getColumnIndex(AnimeContract._GENRE)));
                anime.setScore(c.getDouble(c.getColumnIndex(AnimeContract._SCORE)));
                anime.setCoverImage(c.getString(c.getColumnIndex(AnimeContract._COVERIMAGE)));
                anime.setEpisodeCount(c.getString(c.getColumnIndex(AnimeContract._EPISODECOUNT)));
                anime.setStartDate(c.getString(c.getColumnIndex(AnimeContract._STARTDATE)));
                anime.setEndDate(c.getString(c.getColumnIndex(AnimeContract._ENDDATE)));
                anime.setStatus(c.getString(c.getColumnIndex(AnimeContract._STATUS)));
                anime.setUrl(c.getString(c.getColumnIndex(AnimeContract._URL)));

                list.add(anime);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return list;
    }

    public ArrayList<Anime> getAllAnime() {
        ArrayList<Anime> AnimeList = new ArrayList<Anime>();
        Anime anime;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + AnimeContract.TABLE + " ORDER BY " + AnimeContract._TITLE + " ASC",
                null);
        if (c.moveToFirst()) {
            do {
                anime = new Anime();
                anime.set_id(c.getInt(c.getColumnIndex(AnimeContract._ID)));
                anime.setTitle(c.getString(c.getColumnIndex(AnimeContract._TITLE)));
                anime.setDescription(c.getString(c.getColumnIndex(AnimeContract._DESCRIPTION)));
                anime.setGenre(c.getString(c.getColumnIndex(AnimeContract._GENRE)));
                anime.setScore(c.getDouble(c.getColumnIndex(AnimeContract._SCORE)));
                anime.setCoverImage(c.getString(c.getColumnIndex(AnimeContract._COVERIMAGE)));
                anime.setEpisodeCount(c.getString(c.getColumnIndex(AnimeContract._EPISODECOUNT)));
                anime.setStartDate(c.getString(c.getColumnIndex(AnimeContract._STARTDATE)));
                anime.setEndDate(c.getString(c.getColumnIndex(AnimeContract._ENDDATE)));
                anime.setStatus(c.getString(c.getColumnIndex(AnimeContract._STATUS)));
                anime.setUrl(c.getString(c.getColumnIndex(AnimeContract._URL)));

                AnimeList.add(anime);
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return AnimeList;
    }

    public Anime getLastAddedAnime() {
        Anime anime = new Anime();
        SQLiteDatabase db = getReadableDatabase();
        String query = "Select * FROM " + AnimeContract.TABLE + " ORDER BY " + AnimeContract._ID + " DESC LIMIT 1";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            anime.set_id(c.getInt(c.getColumnIndex(AnimeContract._ID)));
            anime.setTitle(c.getString(c.getColumnIndex(AnimeContract._TITLE)));
            anime.setDescription(c.getString(c.getColumnIndex(AnimeContract._DESCRIPTION)));
            anime.setGenre(c.getString(c.getColumnIndex(AnimeContract._GENRE)));
            anime.setScore(c.getDouble(c.getColumnIndex(AnimeContract._SCORE)));
            anime.setCoverImage((c.getString(c.getColumnIndex(AnimeContract._COVERIMAGE))));
            anime.setEpisodeCount(c.getString(c.getColumnIndex(AnimeContract._EPISODECOUNT)));
            anime.setStartDate(c.getString(c.getColumnIndex(AnimeContract._STARTDATE)));
            anime.setEndDate(c.getString(c.getColumnIndex(AnimeContract._ENDDATE)));
            anime.setStatus(c.getString(c.getColumnIndex(AnimeContract._STATUS)));
            anime.setUrl(c.getString(c.getColumnIndex(AnimeContract._URL)));
        }
        c.close();
        Log.d(TAG, "Anime " + anime.getTitle() + " :getLastAddedAnime()");
        db.close();
        return anime;
    }

    public Anime getAnime(String titleanime) {
        String name = '"' + titleanime + '"';
        SQLiteDatabase db = getReadableDatabase();
        Anime anime = new Anime();
        String query = "SELECT * FROM " + AnimeContract.TABLE + " WHERE " + AnimeContract._TITLE + " = " + name;
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            anime.set_id(c.getInt(c.getColumnIndex(AnimeContract._ID)));
            anime.setTitle(c.getString(c.getColumnIndex(AnimeContract._TITLE)));
            anime.setDescription(c.getString(c.getColumnIndex(Contract.AnimeContract._DESCRIPTION)));
            anime.setGenre(c.getString(c.getColumnIndex(AnimeContract._GENRE)));
            anime.setScore(c.getDouble(c.getColumnIndex(AnimeContract._SCORE)));
            anime.setCoverImage(c.getString(c.getColumnIndex(AnimeContract._COVERIMAGE)));
            anime.setEpisodeCount(c.getString(c.getColumnIndex(AnimeContract._EPISODECOUNT)));
            anime.setStartDate(c.getString(c.getColumnIndex(AnimeContract._STARTDATE)));
            anime.setEndDate(c.getString(c.getColumnIndex(AnimeContract._ENDDATE)));
            anime.setStatus(c.getString(c.getColumnIndex(AnimeContract._STATUS)));
            anime.setUrl(c.getString(c.getColumnIndex(AnimeContract._URL)));
        }
        c.close();
        db.close();
        return anime;

    }

    //End Read---------------

    //Update------------

    //End Update--------

    //Delete------------------

    public boolean deleteAnime(int id) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(AnimeContract.TABLE, AnimeContract._ID + "=" + id, null) > 0;
    }

    public void deleteAll() {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + AnimeContract.TABLE);
        db.close();


    }

    //End Delete------

    public boolean checkIfRecordExists(String title) {
        SQLiteDatabase db = getReadableDatabase();
        String mtitle = '"' + title + '"';
        String query = "Select title From " + AnimeContract.TABLE + " WHERE " + AnimeContract._TITLE + " = " + mtitle;
        Cursor c = db.rawQuery(query, null);
        return c.moveToFirst();

    }

    public boolean checkTable() {
        SQLiteDatabase db = getWritableDatabase();
        String count = "SELECT count(*) FROM anime";
        Cursor mcursor = db.rawQuery(count, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);
        if (icount > 0) {
            return true;
        } else {
            return false;
        }

    }






}
