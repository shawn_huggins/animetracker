package com.hugginsjrshawn.animetracker.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.data.Anime;
import com.hugginsjrshawn.animetracker.data.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Shawn on 1/1/2015.
 */
public class AnimeSearchFragment extends Fragment {
    public static final int REQUESTCODE1 = 1;
    public ArrayAdapter<Anime> adapter;


    public AnimeSearchFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ArrayAdapter<Anime>(getActivity(), R.layout.list_item_new_anime_search,
                R.id.title_text_view, new ArrayList<Anime>());
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_anime_search, container, false);
        final ListView listView = (ListView) rootView.findViewById(R.id.list_view_new_anime);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Anime anime = adapter.getItem(position);
                String title = anime.getTitle();
                DBHelper db = new DBHelper(getActivity());
                Log.d("Checking if entry exists", "...");
                if (db.checkIfRecordExists(anime.getTitle())) {
                    getActivity().setResult(Activity.RESULT_CANCELED);
                    Toast.makeText(getActivity(), getString(R.string.toast_anime_exists), Toast.LENGTH_SHORT).show();
                    Log.d("SETRESULT", "RESUL_FAILEd");
                    getActivity().finish();
                } else {
                    db.addAnime(anime);
                    Intent titleOfAnime = new Intent();
                    titleOfAnime.putExtra(AnimeListFragment.KEY_ID, title);
                    getActivity().setResult(Activity.RESULT_OK, titleOfAnime);
                    Toast.makeText(getActivity(), getString(R.string.toast_anime_added), Toast.LENGTH_LONG).show();
                    Log.d("SETRESULT", "RESULT_OK");
                    getActivity().finish();
                }

            }
        });

        EditText editText = (EditText) rootView.findViewById(R.id.edit_text_anime_search);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                FetchAnimeTask task = new FetchAnimeTask();
                task.execute(text);


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return rootView;

    }

    public class FetchAnimeTask extends AsyncTask<String, Void, ArrayList<Anime>> {

        @Override
        protected ArrayList<Anime> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String animeJson = null;
            String url;


            try {
                url = Uri.parse("http://hummingbird.me/api/v1/search/anime?").buildUpon().
                        appendQueryParameter("query", params[0]).build().toString();
                URL parsedUrl = new URL(url);
                connection = (HttpURLConnection) parsedUrl.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                StringBuilder builder = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String Line;
                while ((Line = reader.readLine()) != null) {
                    builder.append(Line + "\n");
                }
                animeJson = builder.toString();
                Log.d("Do in Background", animeJson);

            } catch (IOException e) {
                Log.e("yey", "ee");

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.e("de", "Error closing stream");
                    }
                }
            }
            Log.d("Fetchanime", animeJson);
            try {
                return parseJsonToAnime(animeJson);
            } catch (JSONException e) {
                Log.e("ParseJson", "Failed to parseJson " + e);
            }
            return null;
        }

        public ArrayList<Anime> parseJsonToAnime(String json) throws JSONException {
            String title;
            String description;
            double score;
            String genre;
            String coverImage;
            String episodeCount;
            String startDate;
            String endDate;
            String status;
            String url;
            if (json == null) {
                return null;
            }
            ArrayList<Anime> fuzzyAnime = new ArrayList<Anime>();
            JSONArray listOfAnime = new JSONArray(json);

            for (int i = 0; i < listOfAnime.length(); i++) {
                JSONObject show = listOfAnime.getJSONObject(i);
                title = show.getString("title");
                description = show.getString("synopsis");
                score = show.getDouble("community_rating");
                coverImage = show.getString("cover_image");

                String count = show.getString("episode_count");
                if (count.equals("null")) {
                    episodeCount = "TBA";
                } else {
                    episodeCount = count;
                }
                if (show.has("start_date")) {
                    if (!show.isNull("start_date")) {
                        startDate = show.getString("start_date");
                        if (show.isNull("end_date")) {
                            endDate = "N/A";
                        } else {
                            endDate = show.getString("end_date");
                        }
                    } else {
                        startDate = "N/A";
                        endDate = "N/A";
                    }
                } else if (show.has("started_airing")) {
                    if (!show.isNull("started_airing")) {
                        startDate = show.getString("started_airing");
                        if (show.isNull("finished_airing")) {
                            endDate = "N/A";
                        } else {
                            endDate = "N/A";
                        }
                    } else {
                        startDate = "N/A";
                        endDate = "N/A";
                    }
                } else {
                    Log.e("error", "json has neither startdate or started airing");
                    return null;
                }


                status = show.getString("status");
                url = show.getString("url");

                if (show.has("genres")) {
                    JSONObject showGenre = show.getJSONArray("genres").getJSONObject(0);
                    genre = showGenre.getString("name");
                } else {
                    genre = "N/A";
                }
                Anime anAnime = new Anime(title, description, score, genre, coverImage,
                        episodeCount, startDate, endDate, status, url);
                fuzzyAnime.add(anAnime);
            }
            return fuzzyAnime;
        }

        @Override
        protected void onPostExecute(ArrayList<Anime> animes) {
            if (adapter != null) {
                adapter.clear();
            }
            if (animes != null) {
                for (Anime a : animes) {
                    adapter.add(a);
                }
            }
        }
    }

}//end fragment
