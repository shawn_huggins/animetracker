package com.hugginsjrshawn.animetracker.activites;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.data.Anime;
import com.hugginsjrshawn.animetracker.data.DBHelper;
import com.hugginsjrshawn.animetracker.fragments.AnimeDetailFragment;

import java.util.ArrayList;


public class SearchActivity extends ActionBarActivity {
    private AnimeAdapter mAdapter;
    private Context mCtx = this;
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mListView = (ListView) findViewById(R.id.listView);
        //mAdapter = new AnimeListFragment.AnimeAdapter(this, R.layout.list_item_animelist, m);
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }


    private void doMySearch(String query) {
        DBHelper db = new DBHelper(this);
        ArrayList<Anime> list = db.getAllAnimeSearch(query);
        TextView textView = (TextView) findViewById(R.id.text_view);
        if (list.size() > 0){
            mAdapter = new AnimeAdapter(this, R.layout.list_item_animelist, list);
            mListView.setAdapter(mAdapter);
            textView.setText(getString(R.string.results_found));
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Anime anime = mAdapter.getItem(position);
                    int _id = anime.get_id();
                    Intent i = new Intent(mCtx, DetailAnimeActivity.class);
                    i.putExtra(AnimeDetailFragment.KEY_ID, _id);
                    startActivity(i);
                }
            });
        } else{
            textView.setText(getString(R.string.results_not_found));

        }
    }



    public class AnimeAdapter extends ArrayAdapter<Anime> {

        private int layout;

        AnimeAdapter(Context c, int layout, ArrayList<Anime> anime) {
            super(c, layout, anime);
            this.layout = layout;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = getLayoutInflater().inflate(layout, null);
            }

            Anime anime = getItem(position);

            TextView textView = (TextView) row.findViewById(R.id.title_text_view);
            textView.setText(anime.getTitle());

            TextView scoreTextView = (TextView) row.findViewById(R.id.score_text_view);

            scoreTextView.setText(anime.getStringScore());

            return row;
        }


        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    }
}
