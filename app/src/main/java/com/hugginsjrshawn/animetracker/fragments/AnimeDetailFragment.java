package com.hugginsjrshawn.animetracker.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.data.Anime;
import com.hugginsjrshawn.animetracker.data.DBHelper;

/**
 * Created by Shawn on 1/1/2015.
 */
public class AnimeDetailFragment extends Fragment {
    public static final String KEY_ID = "animedetailfragment.key_id";
    private static final String TAG = "DetailFragment";
    private Bundle bundle;
    private Anime detailAnime;

    public AnimeDetailFragment() {
    }

    public static AnimeDetailFragment newInstance(int _id) {
        AnimeDetailFragment fragment = new AnimeDetailFragment();
        Bundle args = new Bundle();
        args.putInt("id", _id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        bundle = getArguments();
        if (bundle != null) {
            int _id = bundle.getInt("id");
            DBHelper db = new DBHelper(getActivity());
            detailAnime = db.getAnimeById(_id);
        }
        Log.d(TAG, "End of onCreate reached");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_detail, container, false);
        if (bundle != null) {

            TextView scoreTextview = (TextView) v.findViewById(R.id.score_text_view);
            scoreTextview.setText(detailAnime.getStringScore());

            TextView titleTextview = (TextView) v.findViewById(R.id.title_text_view);
            titleTextview.setText(detailAnime.getTitle());

            TextView infoTextView = (TextView) v.findViewById(R.id.additional_info);
            infoTextView.setText(detailAnime.getAdditionalInfo());

            TextView synopsisTextView = (TextView) v.findViewById(R.id.synopsis);
            synopsisTextView.setText(detailAnime.getDescription());
        } else {
            return null;
        }

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_detail_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_browser:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(detailAnime.getUrl())));
                return true;
            default:
                return false;
        }
    }
}