package com.hugginsjrshawn.animetracker.data;

import android.provider.BaseColumns;

/**
 * Created by shawnjr on 12/25/14.
 */
public class Contract {
    public static String DB_NAME = "anime_tracker.db";
    public static int DB_VERSION = 7;

    public class AnimeContract implements BaseColumns {
        public static final String TABLE = "anime_tracker";

        public static final String _ID = BaseColumns._ID;
        public static final String _TITLE = "title";
        public static final String _DESCRIPTION = "description";
        public static final String _GENRE = "genre";
        public static final String _SCORE = "score";
        public static final String _COVERIMAGE = "coverimage";
        public static final String _EPISODECOUNT = "episodecount";
        public static final String _STARTDATE = "startdate";
        public static final String _ENDDATE = "enddate";
        public static final String _STATUS = "status";
        public static final String _URL = "url";


    }
}
