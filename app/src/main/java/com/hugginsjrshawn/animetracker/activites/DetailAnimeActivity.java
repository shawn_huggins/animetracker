package com.hugginsjrshawn.animetracker.activites;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.fragments.AnimeDetailFragment;

public class DetailAnimeActivity extends ActionBarActivity {
    private final static String TAG = "DetailAnimeActivity";
    private int _id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        _id = getIntent().getIntExtra(AnimeDetailFragment.KEY_ID, 0);
        Log.d(TAG, "onCreate reached");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, AnimeDetailFragment.newInstance(_id))
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
