package com.hugginsjrshawn.animetracker.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hugginsjrshawn.animetracker.R;
import com.hugginsjrshawn.animetracker.activites.NewAnimeSearchActivity;
import com.hugginsjrshawn.animetracker.data.Anime;
import com.hugginsjrshawn.animetracker.data.DBHelper;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * A placeholder fragment containing a simple view.
 */
public class AnimeListFragment extends ListFragment {
    public static final String KEY_ID = "animelistfragment.keyid";
    private static final String TAG = "AnimeListFragment";
    private ListView mListView;
    private AnimeAdapter mAdapter;
    private onAnimeSelectedListener mListener;
    private DBHelper db;
    private ArrayList<Anime> mAnimeList;


    public AnimeListFragment() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String TAG = "onActivityResult";
        if (requestCode != AnimeSearchFragment.REQUESTCODE1) {
            Log.d(TAG, "REQUESTCODE failed");
            return;
        }
        Log.d(TAG, "REQUESTCODE passed");
        if (resultCode == getActivity().RESULT_OK) {
            Log.d(TAG, "result_ok reached, calling updateAdapter()");
            String title = data.getStringExtra(KEY_ID);
            updateAddedAdapter(title);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onAnimeSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnAnimeSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DBHelper(getActivity());
        mAnimeList = db.getAllAnime();
        mAdapter = new AnimeAdapter(getActivity(), R.layout.list_item_animelist, mAnimeList);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_view_fragment, container, false);
        mAdapter = new AnimeAdapter(getActivity(), R.layout.list_item_animelist, mAnimeList);
        mListView = (ListView) v.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.contexual_list_item, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int numberDeleted = 0;
                switch (item.getItemId()) {
                    case R.id.menu_item_delete_crime:
                        for (int i = mAdapter.getCount() - 1; i >= 0; i--) {
                            if (mListView.isItemChecked(i)) {
                                DBHelper db = new DBHelper(getActivity());
                                if (db.deleteAnime(mAdapter.getItem(i).get_id())) {
                                    mAdapter.remove(mAdapter.getItem(i));
                                    numberDeleted++;
                                }
                            }
                        }
                        mode.finish();
                        Toast.makeText(getActivity(), numberDeleted + getString(R.string.toast_anime_removed),
                                Toast.LENGTH_LONG).show();
                        return true;

                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });


        FloatingActionButton Fab1 = (FloatingActionButton) v.findViewById(R.id.fab1);
        Fab1.attachToListView(mListView);
        Fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetOn(getActivity())) {
                    Intent i = new Intent(getActivity(), NewAnimeSearchActivity.class);
                    startActivityForResult(i, AnimeSearchFragment.REQUESTCODE1);
                } else {
                    Log.e(TAG, "No internet connection");
                    Toast.makeText(getActivity(), getString(R.string.toast_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        FloatingActionButton Fab2 = (FloatingActionButton) v.findViewById(R.id.fab2);
        Fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetOn(getActivity())) {
                    Intent i = new Intent(getActivity(), NewAnimeSearchActivity.class);
                    startActivityForResult(i, AnimeSearchFragment.REQUESTCODE1);
                } else {
                    Log.e(TAG, "No internet connection");
                    Toast.makeText(getActivity(), getString(R.string.toast_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Log.d(TAG, "ListItemClicked");
        Anime anime = mAdapter.getItem(position);
        int _id = anime.get_id();
        mListener.loadAnime(_id);
    }

    public void updateAddedAdapter(String title) {
        Anime iAnime = db.getAnime(title);
        mAdapter.add(iAnime);
        mAdapter.sort(new Comparator<Anime>() {
            @Override
            public int compare(Anime a1, Anime a2) {
                return a1.getTitle().toLowerCase().compareTo(a2.getTitle().toLowerCase());
            }
        });


    }

    public static boolean isInternetOn(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public ArrayList<Anime> getAnimelist() {
        return mAnimeList;
    }

    public interface onAnimeSelectedListener {
        public void loadAnime(int AnimeId);

        ;

    }

    public class AnimeAdapter extends ArrayAdapter<Anime> {

        private int layout;

        AnimeAdapter(Context c, int layout, ArrayList<Anime> anime) {
            super(c, layout, anime);
            this.layout = layout;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = getActivity().getLayoutInflater().inflate(layout, null);
            }

            Anime anime = getItem(position);

            TextView textView = (TextView) row.findViewById(R.id.title_text_view);
            textView.setText(anime.getTitle());

            TextView scoreTextView = (TextView) row.findViewById(R.id.score_text_view);

            scoreTextView.setText(anime.getStringScore());

            return row;
        }
    }
}